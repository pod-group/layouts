[![pipeline status](https://gitlab.com/pod-group/layouts/badges/master/pipeline.svg)](https://gitlab.com/pod-group/layouts/-/commits/master)
# layouts
This repo holds the Snaith Group's solar cell layout design files.  
The very latest design revision can be found here: https://pod-group.gitlab.io/layouts/latest  
Current and historical design release packages are here: https://gitlab.com/pod-group/layouts/-/packages  

## Latest Design Preview
[![Layout Preview](https://pod-group.gitlab.io/layouts/latest/30x30_layout.svg)](https://pod-group.gitlab.io/layouts/latest/30x30_layout.svg)

Note to self: gitlab CI is setup to automate tagged commits into proper releases.
